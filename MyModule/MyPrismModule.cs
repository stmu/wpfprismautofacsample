﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;

namespace MyModule
{
  public class MyPrismModule : IModule
  {
    private readonly IRegionManager _regionManager;

    public MyPrismModule(IRegionManager regionManager)
    {
      _regionManager = regionManager;
    }

    public void Initialize()
    {
      _regionManager.RegisterViewWithRegion("MainView", typeof(UserControl1));
    }
  }

}
