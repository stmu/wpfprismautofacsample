﻿using Autofac;

namespace MyModule
{
  public class AutofacModule : Module
  {
    protected override void Load(ContainerBuilder builder)
    {
      base.Load(builder);

      builder.RegisterType<MyPrismModule>();
      // builder.RegisterType<UserControl1ViewModel>();
      builder.RegisterType<UserControl1>();//.UsingConstructor(typeof(MyViewModel));
    }
  }
}
