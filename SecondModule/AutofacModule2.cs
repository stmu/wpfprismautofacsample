﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using MyModule;

namespace SecondModule
{
  public class AutofacModule2 : Module
  {
    protected override void Load(ContainerBuilder builder)
    {
      base.Load(builder);

      builder.RegisterType<Module2>();
      // builder.RegisterType<UserControl1ViewModel>();
      builder.RegisterType<UserControl1>();//.UsingConstructor(typeof(MyViewModel));
    }
  }

}
