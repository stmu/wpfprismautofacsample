﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using MyModule;

namespace SecondModule
{
  public class Module2 : IModule
  {
    private readonly IRegionManager _regionManager;

    public Module2(IRegionManager regionManager)
    {
      _regionManager = regionManager;
    }

    public void Initialize()
    {
      _regionManager.RegisterViewWithRegion("MainView", typeof(UserControl1));
    }
  }

}
