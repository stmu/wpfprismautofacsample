﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using Autofac;
using Microsoft.Practices.Prism.Modularity;
using Prism.AutofacExtension;

namespace WpfApplication2
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    protected override void OnStartup(StartupEventArgs e)
    {
      var bootstrapper = new Bootstrapper();
      bootstrapper.Run();
    }
  }

  public class Bootstrapper : AutofacBootstrapper
  {
    private Collection<IModuleCatalogItem> _catelogItems = new Collection<IModuleCatalogItem>();

    protected override IModuleCatalog CreateModuleCatalog()
    {
      var catalog = new DirectoryModuleCatalog()
      {
        ModulePath = @".\Modules"
      };

      catalog.Load();
      this._catelogItems = catalog.Items;

      return catalog;
    }

    protected override void ConfigureContainer(ContainerBuilder builder)
    {
      base.ConfigureContainer(builder);
      builder.RegisterType<Shell>();

      // register autofac module
      //builder.RegisterModule<MyModuleConfiguration>();

      // register autofac module from modules folder
     
      DirectoryInfo moduleDir = new DirectoryInfo(@".\Modules");
      foreach (var file in moduleDir.GetFiles("*.dll"))
      {
        var types = Assembly.LoadFrom(file.FullName).GetTypes();
        IEnumerable<Autofac.Module> modulesInAssembly = types.Where(t => typeof(Autofac.Module).IsAssignableFrom(t)).Select(m => (Autofac.Module)Activator.CreateInstance(m));

        foreach (var module in modulesInAssembly)
        {
          builder.RegisterModule(module);
        }
      }


      // Register the module catalog.
      //builder.RegisterTypes(this.catelogItems.Select(item => item.GetType()).ToArray()).AsSelf();
    }

    protected override void ConfigureModuleCatalog()
    {
      base.ConfigureModuleCatalog();
      
      Array.ForEach(this._catelogItems.ToArray(), m => ModuleCatalog.AddModule((ModuleInfo)m));
    }

    protected override DependencyObject CreateShell()
    {
      return Container.Resolve<Shell>();
    }

    protected override void InitializeShell()
    {
      base.InitializeShell();

      Application.Current.MainWindow = (Shell)this.Shell;
      Application.Current.MainWindow.Show();
    }
  }
}
